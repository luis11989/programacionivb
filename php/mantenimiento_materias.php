<?php

include('../conexion/config.php');

if($_GET){
    $accion = $_GET['accion'];
    
    $materias = new materias();
    
    if( $accion=='modificar' || $accion=='nuevo' ){
        $datos = json_decode($_GET['materias'], true);
        echo json_encode( $materias->almacenar() );
        
    } else if($accion=='eliminar'){
        echo json_encode( $materias->eliminar_datos($_GET['idMateria']) );
        
    } else{
        $valor = isset($_GET['valor']) ? $_GET['valor'] : '' ;
        $opcion = isset($_GET['opcion']) ? $_GET['opcion'] : '';
        echo json_encode( $materias->mostrar_datos($opcion, $valor) );
    }
}

class materias{
    private $respuesta = array();
    
    public function almacenar(){
        global $db, $datos, $accion;
        
        if( $accion==='nuevo' ){
            $sql = '
                INSERT INTO materias (codigo, nombre, uv) VALUES(
                    "'. $datos['codigo'] .'",
                    "'. $datos['nombre'] .'",
                    "'. $datos['uv'] .'"
                )
            ';
        } else if( $accion==='modificar' ){
            $sql = '
                UPDATE materias SET 
                    codigo      = "'. $datos['codigo'] .'",
                    nombre      = "'. $datos['nombre'] .'",
                    uv          = "'. $datos['uv'] .'"
                WHERE idMateria = "'. $datos['idMateria'] .'"
            ';
        }
        $db->consulta($sql);
        $resp = $db->obtener_respuesta();
        $msg = $resp===true ? 'exito' : 'error';
        return array('msg'=>$msg); 
    }
    
    public function eliminar_datos($idMateria = 0){
        global $db;
        $db->consulta(' DELETE materias FROM materias WHERE idMateria="'.$idMateria.'"');
        return $this->mostrar_datos();
    }
    
    public function mostrar_datos($opcion='', $valor=''){
        global $db;
        switch($opcion){
            case 'codigo':
                $filtro = ' where codigo like "%'. $valor .'%"';
                break;
            case 'nombre':
                $filtro = ' where nombre like "%'. $valor .'%"';
                break;
            default:
                $filtro = ' ';
                break;
        }
        $db->consulta('
            select materias.idMateria, materias.codigo, 
                materias.nombre, materias.uv
            from materias
            '. $filtro 
        );
        return $db->obtener_datos();
    }
}

?>