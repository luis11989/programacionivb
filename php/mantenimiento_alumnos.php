<?php

include('../conexion/config.php');

if($_GET){
    $accion = $_GET['accion'];
    
    $alumnos = new alumnos();
    
    if( $accion=='modificar' || $accion=='nuevo' ){
        $datos = json_decode($_GET['alumnos'], true);
        echo json_encode( $alumnos->almacenar() );
        
    } else if($accion=='eliminar'){
        echo json_encode( $alumnos->eliminar_datos($_GET['idAlumno']) );
        
    } else{
        $valor = isset($_GET['valor']) ? $_GET['valor'] : '' ;
        $opcion = isset($_GET['opcion']) ? $_GET['opcion'] : '';
        echo json_encode( $alumnos->mostrar_datos($opcion, $valor) );
    }
}

class alumnos{
    private $respuesta = array();
    
    public function almacenar(){
        global $db, $datos, $accion;
        
        if( $accion==='nuevo' ){
            $sql = '
                INSERT INTO alumnos (codigo, nombre, direccion, telefono) VALUES(
                    "'. $datos['codigo'] .'",
                    "'. $datos['nombre'] .'",
                    "'. $datos['direccion'] .'",
                    "'. $datos['tel'] .'"
                )
            ';
        } else if( $accion==='modificar' ){
            $sql = '
                UPDATE alumnos SET 
                    codigo     = "'. $datos['codigo'] .'",
                    nombre     = "'. $datos['nombre'] .'",
                    direccion  = "'. $datos['direccion'] .'",
                    telefono   = "'. $datos['tel'] .'"
                WHERE idAlumno = "'. $datos['idAlumno'] .'"
            ';
        }
        $db->consulta($sql);
        $resp = $db->obtener_respuesta();
        $msg = $resp===true ? 'exito' : 'error';
        return array('msg'=>$msg); 
    }
    
    public function eliminar_datos($idAlumno = 0){
        global $db;
        $db->consulta(' DELETE alumnos FROM alumnos WHERE idAlumno="'.$idAlumno.'"');
        return $this->mostrar_datos();
    }
    
    public function mostrar_datos($opcion='', $valor=''){
        global $db;
        switch($opcion){
            case 'codigo':
                $filtro = ' where codigo like "%'. $valor .'%"';
                break;
            case 'nombre':
                $filtro = ' where nombre like "%'. $valor .'%" OR direccion like "%'.$valor.'%" OR telefono like "%'.$valor.'%" ';
                break;
            default:
                $filtro = ' ';
                break;
        }
        $db->consulta('
            select alumnos.idAlumno, alumnos.codigo, alumnos.nombre, 
                alumnos.direccion, alumnos.telefono
            from alumnos
            '. $filtro 
        );
        return $db->obtener_datos();
    }
}

?>