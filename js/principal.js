$(document).ready(function(e){
    $(".appAlumno").click(function(e){
       $("#dvAlumnos").load("alumnos.html", function(ea){
           $("#btnCerrarAlumnos").click(function(e){
               $("#dvAlumnos").empty();
           });
           $(this).draggable();
       });
    });
    $(".appBuscarAlumnos").click(function(e){
       $("#dvBuscarAlumno").load("buscar_alumnos.html", function(ea){
           $("#btnCerrarBusquedaAlumnos").click(function(e){
               $("#dvBuscarAlumno").empty();
           });
           $(this).draggable();
       });
    });
    $(".appMaterias").click(function(e){
       $("#dvMaterias").load("materias.html", function(ea){
           $("#btnCerrarMaterias").click(function(e){
               $("#dvMaterias").empty();
           });
           $(this).draggable();
       });
    });
    $(".appBuscarMaterias").click(function(e){
       $("#dvBuscarMaterias").load("buscar_materias.html", function(ea){
           $("#btnCerrarBusquedaMaterias").click(function(e){
               $("#dvBuscarMaterias").empty();
           });
           $(this).draggable();
       });
    });
    //CHA
     $(".appAbrirChat").click(function(e){
       $("#dvChat").load("chat.html", function(ea){
           $("#btnCerrarChat").click(function(e){
               $("#dvChat").empty();
           });
           $(this).draggable();
       });
    });
});