//codigo de javascript...
    
document.querySelector("#frmAlumno").addEventListener("submit", function(e){
    e.preventDefault(); 
    guardarAlumnos();
});

function mostrarDatosAlumnos(codigo,nombre,direccion,telefono,id){
    $("#frmAlumno").data("idalumno",id);
    $("#txtCodigoAlumno").val(codigo);
    $("#txtNombreAlumno").val(nombre);
    $("#txtDireccionAlumno").val(direccion);
    $("#txtTelefonoAlumno").val(telefono);
}

function guardarAlumnos() {
    var alumnos = {
        idAlumno  : $("#frmAlumno").data("idalumno"),
        codigo    : document.querySelector("#txtCodigoAlumno").value,
        nombre    : document.querySelector("#txtNombreAlumno").value,
        direccion : document.querySelector("#txtDireccionAlumno").value,
        tel       : document.querySelector("#txtTelefonoAlumno").value
    };
    var accion = $("#frmAlumno").data("idalumno")>0 ? "modificar" : "nuevo";
    fetch("php/mantenimiento_alumnos.php?accion="+ accion +"&alumnos="+ JSON.stringify(alumnos) )
        .then(function(r){
            return r.json();
        })
        .then(function(respuesta){
            //respuesta del servidor...
            var msg = '';
            if( respuesta.msg.indexOf("exito")!==-1 ){
                msg = 'Registro guardado con exito';
                var ele = document.querySelectorAll("#frmAlumno input[type=text]");
                ele.forEach(function(item){
                    item.value="";
                });
                $("#frmAlumno").data("idalumno",0);
                obtener_alumnos('', '');
            } else {
                msg = 'Error desde el servidor';
            }
            document.querySelector("#spnRespuestaAlumno").innerHTML = 
                `Mensaje del servidor: ${msg}`;
        });
}