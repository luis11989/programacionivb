var materias = new Vue({
    el: "#appBusquedaMaterias",
    data : {
        buscar :{
            opcion : "",
            valor  : ""
        },
        materias : []
        
    },
    methods :{
        buscarMaterias: function(){
            var _this = this;
            fetch("php/mantenimiento_materias.php?accion=consulta&opcion="+ this.buscar.opcion +"&valor="+this.buscar.valor )
                .then(function(r){
                    return r.json();
                })
                .then(function(respuesta){
                    _this.materias = respuesta;    
            });
        }
    }, 
    created: function(){
        this.buscarMaterias();
    }
});