var materias = new Vue({
    el: "#appMaterias",
    data : {
        materias :{
            codigo : "",
            nombre : "",
            uv     : ""
        }
        
    },
    methods :{
        guardarMaterias: function(){
            var _this = this;
            fetch("php/mantenimiento_materias.php?accion=nuevo&materias="+ JSON.stringify( this.materias ) )
                .then(function(r){
                    return r.json();
                })
                .then(function(respuesta){
                    if( respuesta.msg.indexOf("exito")>=0 ){
                        _this.materias.codigo = "";
                        _this.materias.nombre = "";
                        _this.materias.uv = "";
                    }
                
            });
        }
    }
});