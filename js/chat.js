var socket = io.connect("http://localhost:3000", {'forceNew':true});
var chat = new Vue({
    el: "#appChat",
    data : {
        msg  : "",
        msgs : []
    },
    methods :{
        enviarMessage: function(){
            socket.emit('newMessage', this.msg);//hago una peticion al servidor node
            this.msg = "";
        }
    },
    created:function(){
        socket.emit('chatHistorial');//hago una peticion al servidor node
    }
});
socket.on('newMessage', function(msg){
    console.log(msg);
    chat.msgs.push(msg);
});
socket.on('chatHistorial', function(msgs){
    msgs.forEach(function(item, index){
        chat.msgs.push(item.msg); 
    });
});