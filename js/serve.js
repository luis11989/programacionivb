var http = require('http').Server(),
    io   = require('socket.io')(http),
    MongoClient = require('mongodb').MongoClient,
    url = 'mongodb://localhost:27017';
 
const dbName = 'chatPrograIV';//DB

io.on("connection", function(socket){
    socket.on('newMessage',function(msg){    
        //conexion a MONGODB
        // Use connect method to connect to the server
        MongoClient.connect(url, function(err, client) {
            console.log("Conectado satisfactoriamente al server");

            const db = client.db(dbName);
            const insertDocuments = function(db) {
                // Get the documents collection
                const collection = db.collection('chat');
                // Insert some documents
                collection.insert({'msg':msg});
                console.log("registro insertado con exito.");
            }
            insertDocuments(db,null);
            //client.close();
        });
        //END conexion
        io.emit('newMessage',msg);
    });
    socket.on('chatHistorial', function(){
        //conexion a MONGODB
        // Use connect method to connect to the server
        MongoClient.connect(url, function(err, client) {
            console.log("Conectado satisfactoriamente al server");
            const db = client.db(dbName);
            
            const findDocuments = function(db) {
                // Get the documents collection
                const collection = db.collection('chat');
                // Find some documents
                collection.find({}).toArray(function(err, msgs) {
                    io.emit('chatHistorial',msgs);
                });
            }
            findDocuments(db);
        });
        //END conexion
    });
});

http.listen(3000, function(){
   console.log("servidor corriendo en el puerto 3000."); 
});