$(document).ready(function(e){
    $("#txtValorAlumno").keyup(function(e){
        obtener_alumnos( $(this).val(), $("#cboBuscarAlumno").val() )
    });
});

obtener_alumnos('','');
 
function obtener_alumnos(valor, opcion){
    $.get(`php/mantenimiento_alumnos.php?accion=consulta&valor=${valor}&opcion=${opcion}`,function(respuesta){
        mostrarAlumnos(respuesta);
    }, "json");
}

function mostrarAlumnos(datos){
    var filas = '';
     $("#tblBusquedaAlumnos > tbody tr").remove();
    $.each(datos, function(index, item){
        filas += `
            <tr data-idalumno="${item.idAlumno}">
                <td onClick="mostrarDatosAlumnos('${item.codigo}','${item.nombre}','${item.direccion}','${item.telefono}','${item.idAlumno}')">${item.codigo}</td>
                <td onClick="mostrarDatosAlumnos('${item.codigo}','${item.nombre}','${item.direccion}','${item.telefono}','${item.idAlumno}')">${item.nombre}</td>
                <td>${item.direccion}</td>
                <td>${item.telefono}</td>
                <td>
                    <a href="javascript:;" class="btn btn-danger" onclick="eliminarAlumno(${item.idAlumno})">Eliminar</a>
                </td>
            </tr>
        `;
    });
    $("#tblBusquedaAlumnos > tbody").append(filas);
}

function eliminarAlumno(idAlumno){
    if( idAlumno>0 ){
        if( confirm("Esta seguro de eliminar el alumno?") ){
            $.get(`php/mantenimiento_alumnos.php?accion=eliminar&idAlumno=${idAlumno}`, function(respuesta){
               mostrarAlumnos(respuesta); 
            }, "json");
        }
    }
}