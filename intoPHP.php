<?php
date_default_timezone_set('America/El_Salvador');
//Esto es codigo de PHP 
//echo 'Hola Mundo desde PHP';
$miVariable = 5;
echo 'El valor de miVariable es: '.$miVariable;
  
$miVariable = ' Saluda...';
echo '<br/> mi saludo: '.$miVariable;

$num1 = 7;
$num2 = 10.5;
$respuesta = $num1 + $num2;
echo $respuesta;

$varBool = true;
echo '<br/> '.$varBool;

$varBool = false;
echo '<br/> varBool = false: '.$varBool;

$hoy = date('d-m-Y H:i:s');
echo '<br/>Hoy es: '.$hoy;

echo '<br/>';
$miMatriz = array(
    'a'=>1,
    'b'=>5,
    'c'=>7,
    'd'=>8,
    'e'=>9
);
echo $miMatriz['d'];
print_r($miMatriz);

$persona = array(
    'dui'       =>'00000000-0',
    'nombre'    => 'Luis',
    'direccion' => 'Usulutan',
    'telefonos' => array(
        'casa'    => '2610-0000',
        'empresa' => '2615-0000',
        'movil'   => '7200-000'
    )
);

echo $persona['nombre'] . ' ' . $persona['telefonos']['movil'];

$json = json_encode( $persona );
echo $json;
print_r( json_decode($json, true) );







?>